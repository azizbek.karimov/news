import { YoutubeFilled, FacebookFilled, TwitterOutlined, InstagramOutlined } from '@ant-design/icons';
import "./footer.scss";
import "./footer.css";
import { Link } from "react-router-dom"

function Footer(){
    return(
        <section className='footer-section'>
            <footer>
                <div classNameName="first-part">
                    <div className="contact-us">
                        <p className="topic">Contact us</p>
                        <p className="text">623 Harrison St., 2nd Floor, San Francisco, CA 94107</p>
                        <p className="text">415-201-6370 hello@commit.com</p>
                    </div>
                    <div className="contact-icons">
                        <Link to=""><YoutubeFilled/></Link>
                        <Link to=""><FacebookFilled/></Link>
                        <Link to=""><TwitterOutlined/></Link>
                        <Link to=""><InstagramOutlined/></Link>
                    </div>
                </div>
                <div className="second-part">
                    <div className="account">
                        <p className="topic">Account</p>
                        <p className="text">Create account</p>
                        <p className="text">Sing in</p>
                        <p className="text">iOS app</p>
                        <p className="text">Android app</p>
                    </div>
                    <div className="company">
                        <p className="topic">Company</p>
                        <p className="text">About Omnifood</p>
                        <p className="text">For Business</p>
                        <p className="text">Cooking partners</p>
                        <p className="text">Careers</p>
                    </div>
                    <div className="resources">
                        <p className="topic">Resources</p>
                        <p className="text">Recipe directory</p>
                        <p className="text">Help center</p>
                        <p className="text">Privacy & terms</p>
                    </div>
                </div>
            </footer>
            <div className="copyright">
                        <div className="text">
                            Copyright © 2022 by Commit, Inc. All rights reserved.
                        </div>
                    </div>
        </section>
    )
}

export default Footer;