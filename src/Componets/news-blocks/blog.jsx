import "./blog.scss";
import { Link } from "react-router-dom";



function Blog(props) {
  const content = props.posts.map((post) =>
    <div key={post.id}>
      <div className="card">
        <div className='img-format'>
          <img src={post.imgLink}></img>
        </div>
        <div className="news-topic">
          <Link to={`/${post.id}`}><h4 className="topic">{post.topic}</h4></Link>
        </div>
        <div className="time">{post.time}</div>
      </div>
    </div>
  );
  return (
    <div className="block container">
      <h3>The Lastest</h3>
      <div className="content">{content}</div>
    </div>
  );
}

export default Blog;