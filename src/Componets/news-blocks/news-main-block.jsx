import "../navbar/news/news.scss";
import { CommentOutlined, ShareAltOutlined, LikeOutlined } from "@ant-design/icons";
import { Col, Row } from 'antd';
import { Link } from "react-router-dom";
import newsData from "../../Data";

export default function NewsMainBlock() {
    return(
        <>
        <Row className='container'>
            <Col span={8}>
                <div>
                    <img className="img-f" src={newsData[0].Education[1].imgLink}></img>
                </div>
            </Col>
        <Col span={16}>
          <div className='news-text'>
            <h4>
              <Link to='/detail' className='item'>{newsData[0].Education[1].topic}</Link>
            </h4>
            <p>
              {newsData[0].Education[1].introText}
              <Link to='/detail' className='readMore'>Читать дальше...</Link>
            </p>
            <div className="sub-text">
              <div className="data">
                <b>{newsData[0].Education[1].time}</b>
              </div>
              <div className="l-c-sh">
                <Link to=''><LikeOutlined/></Link>
                <Link to=''><CommentOutlined /></Link>
                <Link to=''><ShareAltOutlined /></Link>
              </div>
            </div>
          </div>
        </Col>
      </Row>
        <div className="container">
        <hr className='container'></hr>
     </div>
        </>
    )
};