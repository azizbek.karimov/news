import React from "react";
import { Link } from "react-router-dom"

function Navbar(){
  return(
    <section className="navbar">
      <div className='line'></div>
      <div className="navs">
        <Link to="/" >Last News</Link>
        <Link to="/education" >Education</Link>
        <Link to="/sport" >Sport</Link>
        <Link to="/helth" >Helth</Link>
        <Link to="/technology" >Technology</Link>
        <Link to="/programming" >Programming</Link>
        <Link to="/others" >Others</Link>
      </div>
    </section>
  ) 
}


export default Navbar;