import './sameNews.css'
import { Col, Row } from 'antd';
import newsData from '../../../Data';



function SameNews () {
    return(
        <div className="container">
            <h3>Похожие новести</h3>
            <Row className='row'>
                <Col span={8}>
                    <div className='img-format'>
                        <img src='https://st-0.akipress.org/st_limon/4/1660561642.jpg'></img>
                    </div>
                    <h5>Гордость страны! Три бронзы взяли школьники Кыргызстана на международной IT-олимпиаде</h5>
                </Col>
                <Col span={8}>
                    <div className='img-format'>
                        <img src='https://st-0.akipress.org/st_runews/.storage/limon3/images/APREEEEEL22/4debe3984f3ef00993bff385e972e884.jpg'></img>
                    </div>
                    <h5>{newsData[0].Education[1].topic}</h5>
                </Col>
                <Col span={8}>
                <div className='img-format'>
                        <img src='https://media.macphun.com/img/uploads/customer/how-to/579/15531840725c93b5489d84e9.43781620.jpg?q=85&w=1340'></img>
                    </div>
                    <h5>Лайфхак дня: три необходимых навыка в условиях постоянных перемен</h5>
                </Col>
            </Row>
        </div>
    )
}
 


export default SameNews;