import React from "react";
import "./others.scss"

import { Link } from "react-router-dom";
import { AiOutlineHeart, AiFillHeart } from 'react-icons/ai';

export default function Others(props){
    function myFunction(x){
        return
    }

    const content = props.others.map((post) =>
      <div key={post.id}>
        <div className="card">
          <div className='img-format'>
            <img src={post.imgLink}></img>
          </div>
          <div className="news-topic">
            <Link to={`/${post.id}`}><h4 className="topic">{post.topic}</h4></Link>
          </div>
          <div className="time pre-block">{post.time} <Link onclick="myFunction(this)" to=""><AiOutlineHeart/></Link></div>
        </div>
      </div>
    );
    return (
      <div className="block container">
        <h3 className="container news-item">The Lastest</h3>
        <div className="content">{content}</div>
      </div>
    );
  }