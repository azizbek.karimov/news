import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';
import Header from './Componets/header/header';
import Navbar from './Componets/navbar/navbar';
import {BrowserRouter, Link, Route, Routes } from "react-router-dom";
import News from './Componets/navbar/news/news';
import Education from './Componets/navbar/education/education';
import Helth from './Componets/Helth/helth';
import Sport from './Componets/navbar/Sport/sport';
import News1 from './Componets/All news/news1';
import Footer from './Componets/footer/footer';
import Others from './Componets/navbar/others/others';
import Technology from "./Componets/navbar/technology/technology"
import Programming from './Componets/navbar/programming/programming'
import About from './Componets/about/about';
import { AiOutlineMenuUnfold } from 'react-icons/ai';
import Blog from './Componets/news-blocks/blog';
import newsData from './Data';
import News2 from './Componets/All news/news2';
import News3 from "./Componets/All news/news3"
import News4 from "./Componets/All news/news4"
import News5 from "./Componets/All news/news5"

function App() {

  const education = newsData[0].Education;
  const sport = newsData[1].Sport;
  const helth = newsData[2].Helth;
  const technology = newsData[3].Technology;
  const programming = newsData[4].Programming;
  const others = newsData[5].Others;


  
  return (
    <div>
      <BrowserRouter className="rel">
        <div className='head'>
          <Header/>
          <Navbar />
          <div className="menu">
            <AiOutlineMenuUnfold className='menu-icon' size={25}/>
            <div className='categories pos-abs'>
              <Link className='category' to="/" ><h4>Главная</h4></Link>
              <Link className='category' to="/education" ><h4>Образование</h4></Link>
              <Link className='category' to="/sport" ><h4>Спорт</h4></Link>
              <Link className='category' to="/helth" ><h4>Здоровье</h4></Link>
              <Link className='category' to="/technology" ><h4>Технология</h4></Link>
              <Link className='category' to="/programming" ><h4>Программирование</h4></Link>
              <Link className='category' to="/others"><h4>Другие</h4></Link>
            </div>
          </div>
        </div>
        
        <Routes className="news">
          <Route exact path="/" element={<News others={others} education={education}/>}></Route>
          <Route exact path="/education" element={<Education education={education} />}></Route>
          <Route exact path="/helth" element={<Helth helth={helth} />}></Route>
          <Route exact path="/sport" element={<Sport sport={sport} />}></Route>
          <Route exact path="/programming" element={<Programming programming={programming} />}></Route>
          <Route exact path="/technology" element={<Technology technology={technology} />}></Route>
          <Route exact path="/others" element={<Others others={others} />}></Route>

          <Route exact path="/detail" element={<News1/>}></Route>
          <Route exact path="/about" element={<About/>}></Route>
          <Route exact path="/blog" element={<Blog education={education} />}></Route>
          
          <Route exact path="/100" element={<News2/>}></Route>
          <Route exact path="/101" element={<News1/>}></Route>
          <Route exact path="/102" element={<News3/>}></Route>
          <Route exact path="/103" element={<News4/>}></Route>
          <Route exact path="/104" element={<News5/>}></Route>
        </Routes>
        <div className='footer'>
          <Footer/>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
