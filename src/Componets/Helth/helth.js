import React from 'react';
import newsData from '../../Data';
import { Col, Row } from 'antd';
import { Link } from "react-router-dom";
import "./helth.scss"
import { AiOutlineHeart, AiFillHeart } from 'react-icons/ai';

const contentStyle = {
    height: '350px',
    color: '#fff',
    lineHeight: '400px',
    textAlign: 'center',
};
// console.log(newsData[2].Helth[0].topic);

export default function Helth(props){
    function myFunction(x){
        return
    }

    const content = props.helth.map((post) =>
      <div key={post.id}>
        <div className="card">
          <div className='img-format'>
            <img src={post.imgLink}></img>
          </div>
          <div className="news-topic">
            <Link to={`/${post.id}`}><h4 className="topic">{post.topic}</h4></Link>
          </div>
          <div className="time pre-block">{post.time} <Link onclick="myFunction(this)" to=""><AiOutlineHeart/></Link></div>
        </div>
      </div>
    );
    return (
      <div className="block container">
        <h3 className="container news-item">The Lastest</h3>
        <div className="content">{content}</div>
      </div>
    );
  }