import { YoutubeFilled, InstagramOutlined, TwitterOutlined, FacebookFilled, WhatsAppOutlined } from "@ant-design/icons";
// import styles from "../icons/style.module.scss"
import "./style.module.scss";
import "./style.module.css";
// import "../../App.scss"; 



function Icons(){
    return(
        <div className="icons">
            <a className="icon" href="#"><YoutubeFilled /></a>
            <a className="icon" href="#"><InstagramOutlined /></a>
            <a className="icon" href="#"><TwitterOutlined /></a>
            <a className="icon" href="#"><FacebookFilled /></a>
            <a className="icon" href="#"><WhatsAppOutlined /></a>
        </div>
    )
}

export default Icons;