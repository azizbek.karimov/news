import Icons from "../icons/icons"
import Licon from "../icons/Licon";
import { Button } from 'antd';

import { Select } from 'antd';
import { Link } from "react-router-dom";

const { Option } = Select;
const handleChange = (value) => {
  console.log(`selected ${value}`);
};


function Header(){
    return (
        <header className='header'>
            <Icons/>
            <Link to="/" className="h1"><h1>Commit</h1></Link>
            <div className="right-side">
                <Licon className='licon'/> 
                <div className="language">
                <Select
                    defaultValue="en"
                    style={{
                        width: 60,
                    }}
                    onChange={handleChange}
                    >
                    <Option value="en">En</Option>
                    <Option value="ru">Ru</Option>
                    <Option value="kg">Kg</Option>
                </Select>
                </div>
                <Button type="primary">SUBSCRIBE</Button>
            </div>
        </header>
    )
}

export default Header;