import newsData from '../../Data';
import Comm from '../comment/comment';
import SameNews from '../navbar/same news/sameNews';
import "./allNews.scss";

function News1() {
    return(
        <div className="detail my-container sm-continer">
            <div className="my-container">
                <h2 className="detail-item">
                    {newsData[0].Education[1].topic}
                </h2>
                <div className="detail-text">
                    {newsData[0].Education[1].introText}
                </div>
                <div className="img my-container">
                    <div>
                        <img className="img-f" src={newsData[0].Education[1].imgLink}></img>
                    </div>
                </div>
                <div className="detail-text">
                    {newsData[0].Education[1].text}
                </div>
            </div>
            <div className="add-comment">
                <Comm />
            </div>
            <div className='sameNews'>
                <SameNews/>
            </div>
        </div>
    )
}

export default News1;