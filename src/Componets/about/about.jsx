import "./about.scss";
import { BsWhatsapp, BsInstagram, } from 'react-icons/bs';
import { BiMailSend } from 'react-icons/bi';
import { Link } from "react-router-dom";

export default function About(){
    return(
        <>
            <div className="about-us">
                <h2>Commit</h2>
                <div className="img"></div>
                <div className="text">
                    <p>
                        This is news website. And it here you can read a lastest news.
                        And there are categories like: sport, helth, technology, education etc.

                    </p>
                </div>
                <div className="contact">
                    <h4 className="contact-text">Contact us:</h4>
                    <div className="to-contact">
                        <div className="way">
                            <div className="icon"><BsWhatsapp size={25}/></div>
                            <Link to="" className="link">+996776145977</Link>
                        </div>
                        <div className="way">
                            <div className="icon"><BsInstagram size={25}/></div>
                            <Link to="" className="link">@commit</Link>
                        </div>
                        <div className="way">
                            <div className="icon"><BiMailSend size={25}/></div>
                            <Link to="" className="link">commit@gmail.com</Link>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}