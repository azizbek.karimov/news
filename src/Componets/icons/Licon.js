import React from 'react';
import 'antd/dist/antd.css';
import { AudioOutlined } from '@ant-design/icons';
import { Input, Space } from 'antd';

const { Search } = Input;
const suffix = (
  <AudioOutlined
    style={{
      fontSize: 16,
      color: '#1890ff',
    }}
  />
);

const onSearch = (value) => console.log(value);

const Licon = () => (
  <Space direction="vertical">
    <Search
      placeholder="search text"
      allowClear
      onSearch={onSearch}
      style={{
        width: 140,
      }}
    />
  </Space>
);

export default Licon;